<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopulateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('populate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bulan');
            $table->string('nama_desa');
            $table->integer('rw');
            $table->integer('rt');
            $table->string('nomor_kk');
            $table->integer('pdd_awal_bulan_lk');
            $table->integer('pdd_awal_bulan_pr');
            $table->integer('pdd_awal_bulan_jml');
            $table->integer('lahir_lk');
            $table->integer('lahir_pr');
            $table->integer('lahir_jml');
            $table->integer('mati_lk');
            $table->integer('mati_pr');
            $table->integer('mati_jml');
            $table->integer('datang_lk');
            $table->integer('datang_pr');
            $table->integer('datang_jml');
            $table->integer('pindah_lk');
            $table->integer('pindah_pr');
            $table->integer('pindah_jml');
            $table->integer('pdd_akhir_bulan_lk');
            $table->integer('pdd_akhir_bulan_pr');
            $table->integer('pdd_akhir_bulan_jml');
            $table->integer('jumlah_kk_akhir');
            $table->integer('wajib_ktp');
            $table->integer('sudah_ktp');
            $table->integer('belum_ktp');
            $table->integer('sudah_akta_lahir');
            $table->integer('belum_akta_lahir');
            $table->integer('kawin_non_muslim_n');
            $table->integer('kawin_non_muslim_t');
            $table->integer('kawin_non_muslim_r');
            $table->integer('orang_asing');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('populate');
    }
}
